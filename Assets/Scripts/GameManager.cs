﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public enum GameState
{
	Pregame,
	Started,
	Ended,
}

public class GameManager : MonoSingleton<GameManager>
{
	public GameState gameState = GameState.Pregame;
	public BaseShip playerShipPrefab;
	public BaseAsteroid asteroidPrefab;

	public Transform playerSpawnPoint;
	public Transform[] asteroidSpawnPoints;

	public BaseShip playerShip { get; private set; }
	public AudioSource audioSource { get; private set; }
	//private Coroutine asteroidSpawnCoroutine;

	[SerializeField]
	private PlayerInput playerInput;

	public Action GameStartEvent;
	public Action GameEndEvent;
	public Action<int> ScoreChangedEvent;
	public Action<int> HighScoreChangedEvent;

	public List<BaseAsteroid> activeAsteroids = new List<BaseAsteroid>();

	private int currentHighScore = 0;
	public int CurrentHighScore
	{
		get { return currentHighScore; }
		set
		{
			currentHighScore = value;
			if (HighScoreChangedEvent != null)
				HighScoreChangedEvent(currentHighScore);

			PlayerPrefs.SetInt("highscore", currentHighScore);
			PlayerPrefs.Save();
		}
	}

	private int currentScore = 0;
	public int CurrentScore
	{
		get { return currentScore; }
		set
		{
			currentScore = value;
			if (ScoreChangedEvent != null)
				ScoreChangedEvent(currentScore);

			if (CurrentHighScore < currentScore)
				CurrentHighScore = currentScore;
		}
	}

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	private void Start()
	{
		UIManager.instance.RegisterEvents();
		CurrentHighScore = PlayerPrefs.GetInt("highscore", 0);
	}

	private void Update()
	{
		if(gameState != GameState.Started)
		{
			if(Input.GetButtonDown("Submit"))
			{
				StartGame();
			}
		}
	}

	public void StartGame()
	{
		gameState = GameState.Started;

		// If the ship is already assigned we are restarting and should clean everything up
		if (playerShip != null)
		{
			SimplePool.Despawn(playerShip.gameObject);
		}

		playerShip = SimplePool.Spawn(playerShipPrefab.gameObject, playerSpawnPoint.position, playerSpawnPoint.rotation).GetComponent<BaseShip>();
		playerInput.playerShip = playerShip;
		playerShip.tag = "Player";

		activeAsteroids.Clear();

		if (GameStartEvent != null)
			GameStartEvent();

		StartCoroutine(SpawnAsteroid());
		playerShip.OnSpawn();
		CurrentScore = 0;
	}

	public void EndGame()
	{
		gameState = GameState.Ended;

		if (GameEndEvent != null)
			GameEndEvent();
	}

	IEnumerator SpawnAsteroid()
	{
		while (gameState == GameState.Started)
		{
			yield return new WaitForSeconds(UnityEngine.Random.Range(2.0f, 3.0f));
			//Debug.Log("Spawn " + activeAsteroids.Where(a => a.Size >= 3).Count());
			if (activeAsteroids.Where(a => a.Size >= 3).Count() < 2)
			{
				Transform spawnPoint = GetAsteroidSpawnPoint();
				BaseAsteroid asteroid = SimplePool.Spawn(asteroidPrefab.gameObject, spawnPoint.position, spawnPoint.rotation).GetComponent<BaseAsteroid>();
				asteroid.OnSpawn(3, null);
			}
		}
	}

	private Transform GetAsteroidSpawnPoint()
	{
		return asteroidSpawnPoints[UnityEngine.Random.Range(0, asteroidSpawnPoints.Length)];
	}
}
