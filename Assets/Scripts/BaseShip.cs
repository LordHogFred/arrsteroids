﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class BaseShip : BaseEntity
{
	/// <summary>
	/// Speed of ship
	/// </summary>
	public float speed = 200.0f;

	/// <summary>
	/// Turning rate of ship
	/// </summary>
	public float turnRate = 1.0f;

	/// <summary>
	/// Cannons on the ship
	/// </summary>
	public Cannon[] cannons;

	/// <summary>
	/// The point on the ship to which movement force is applied
	/// </summary>
	[SerializeField]
	private Transform forcePoint;

	/// <summary>
	/// Rate of fire for the ships cannons (could move this on to cannons themselves for upgrade purposes?)
	/// </summary>
	public float fireRate = 1.0f;
	private float fireDelay;

	protected override void Awake()
	{
		base.Awake();

		// Set up cannons
		foreach(Cannon cannon in cannons)
		{
			cannon.owningShip = this;
		}
	}

	private void Update()
	{
		if(fireDelay > 0)
			fireDelay -= Time.deltaTime;
	}

	/// <summary>
	/// Handle ship movement
	/// </summary>
	/// <param name="input"></param>
	public void HandleMovement(Vector3 input)
	{
		// Disable movement handling if the ship is no longer alive
		if (!Alive)
			return;

		// Turn towards the input direction to simulate inertia
		Vector3 movementDirection = Vector3.RotateTowards(transform.forward, input, turnRate * Time.deltaTime, 0);
		transform.rotation = Quaternion.LookRotation(movementDirection);

		// Apply force at the rear of the ship
		rigidbody.AddForceAtPosition(transform.forward * input.magnitude * speed * Time.deltaTime, forcePoint.position);
	}

	/// <summary>
	/// Fire a cannon group based on a direction given relative to the ship
	/// </summary>
	/// <param name="direction"></param>
	public void FireCannons(Vector3 direction)
	{
		CannonGroup cannonGroup = CannonGroup.Front;

		float angle = Vector3.SignedAngle(transform.forward, direction.normalized, Vector3.up);

		if (angle <= -10.0f)
			cannonGroup = CannonGroup.Left;
		else if (angle >= 10.0f)
			cannonGroup = CannonGroup.Right;

		FireCannons(cannonGroup);
	}

	/// <summary>
	/// Fire a specified cannon group
	/// </summary>
	/// <param name="group"></param>
	public void FireCannons(CannonGroup group)
	{
		if (!Alive)
			return;

		// If cannons are ready fire all cannons in chosen fire group
		if (fireDelay <= 0)
		{
			fireDelay = fireRate;
			Cannon[] cannonsToFire = cannons.Where(c => c.cannonGroup == group).ToArray();
			if (cannonsToFire.Length > 0)
			{
				foreach (Cannon cannon in cannonsToFire)
				{
					cannon.Fire();
				}
				PlaySoundEffect("CannonFire");
			}
		}
	}

	/// <summary>
	/// Deal damage to the ship
	/// </summary>
	/// <param name="damage"></param>
	public override void Damage(int damage)
	{
		Health -= damage;
		PlaySoundEffect("Impact");
	}

	/// <summary>
	/// Kill and sink the shi[
	/// </summary>
	public override void Kill()
	{
		GameManager.instance.EndGame();

		collider.isTrigger = true;
		rigidbody.velocity = Vector3.zero;
		rigidbody.isKinematic = true;
		animController.Play("ShipSink");
		PlaySoundEffect("Sink");
		PlaySoundEffect("Despawn");
	}

	/// <summary>
	/// Set up default values for ship when it is spawned
	/// </summary>
	public void OnSpawn()
	{
		Health = MaxHealth;
		collider.isTrigger = false;
		rigidbody.velocity = Vector3.zero;
		rigidbody.isKinematic = false;
		animController.Play("ShipIdle");
		PlaySoundEffect("Spawn");
	}

	//public override void Teleport(Vector3 newPosition)
	//{
	//	StartCoroutine(TeleportDelay(newPosition));
	//}

	// Play an exit/enter anim when wrapping around map, didn't look/feel great so decided to not use it
	public IEnumerator TeleportDelay(Vector3 newPosition)
	{
		animController.Play("ShipShrinkOut");
		yield return new WaitForSeconds(0.1f);
		base.Teleport(newPosition);
	}
}
