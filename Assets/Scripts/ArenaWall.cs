﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaWall : MonoBehaviour
{
	public ArenaWall oppositeWall;
	public Vector3 offset;

	private void OnCollisionEnter(Collision collision)
	{
		TeleportEntity(collision.collider);
	}

	private void OnTriggerEnter(Collider other)
	{
		TeleportEntity(other);
	}

	private void TeleportEntity(Collider entityCollider)
	{
		if (entityCollider.tag != "Terrain")
		{
			Vector3 offsetPosition = offset;

			// Add the collider bounds of the object we are moving to the offset to make sure no additional collisions are made after we move
			if (offsetPosition.x > 0)
				offsetPosition.x -= entityCollider.bounds.extents.magnitude;
			else if (offsetPosition.x < 0)
				offsetPosition.x += entityCollider.bounds.extents.magnitude;

			if (offsetPosition.z > 0)
				offsetPosition.z -= entityCollider.bounds.extents.magnitude;
			else if (offsetPosition.z < 0)
				offsetPosition.z += entityCollider.bounds.extents.magnitude;

			BaseEntity entity = entityCollider.GetComponent<BaseEntity>();
			if (entity != null)
			{
				entity.Teleport(entityCollider.gameObject.transform.position - offsetPosition);
			}
			else
			{
				entityCollider.gameObject.transform.position -= offsetPosition;
			}
		}
	}
}
