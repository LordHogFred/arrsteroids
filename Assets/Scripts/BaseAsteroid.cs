﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BaseAsteroid : BaseEntity
{
	public int Size { get; private set; }
	public int splitInto = 2;
	public float minInitialSpeed = 5.0f;
	public float maxInitialSpeed = 10.0f;
	public int damageValue = 10;

	public int scoreValue { get { return 150 / Size; } }

	[SerializeField]
	private Mesh[] meshes;

	[SerializeField]
	private MeshFilter meshFilter;

	public List<Collider> ignoreColliders = new List<Collider>();
	public float ignoreColliderReset = 0.3f;
	private float ignoreColliderResetTimer = 0;

	protected override void Awake()
	{
		base.Awake();
		GameManager.instance.GameStartEvent += OnGameStart;
	}

	public override void Damage(int damage)
	{
		base.Damage(damage);
	}

	private void Update()
	{
		if (ignoreColliderResetTimer > 0)
		{
			ignoreColliderResetTimer -= Time.deltaTime;
			if(ignoreColliderResetTimer <= 0)
			{
				foreach(Collider c in ignoreColliders)
				{
					Physics.IgnoreCollision(collider, c, false);
				}
			}
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		if(collision.collider.tag == "Player")
		{
			BaseEntity entity = collision.gameObject.GetComponent<BaseEntity>();
			if(entity != null)
			{
				entity.Damage(damageValue);
			}
		}
	}

	public void OnSpawn(int _size, List<Collider> _ignoreColliders)
	{
		// Make sure we don't get 0 size asteroids because then bad things happen
		if (_size < 1)
			_size = 1;

		if (_ignoreColliders != null)
		{
			ignoreColliders = _ignoreColliders;
			ignoreColliderResetTimer = ignoreColliderReset;
			foreach (Collider c in ignoreColliders)
			{
				Physics.IgnoreCollision(collider, c, true);
			}
		}

		Health = MaxHealth;
		meshFilter.mesh = meshes[UnityEngine.Random.Range(0, meshes.Length)];
		Size = _size;
		transform.localScale = new Vector3(_size, _size, _size);
		rigidbody.isKinematic = false;
		rigidbody.velocity = Vector3.zero;
		rigidbody.mass = _size;
		rigidbody.AddForce(transform.forward * UnityEngine.Random.Range(minInitialSpeed, maxInitialSpeed), ForceMode.Impulse);
		GameManager.instance.activeAsteroids.Add(this);
	}

	public void Despawn()
	{
		ignoreColliderResetTimer = 0;
		GameManager.instance.activeAsteroids.Remove(this);
		rigidbody.velocity = Vector3.zero;
		rigidbody.isKinematic = true;

		foreach (Collider c in ignoreColliders)
		{
			Physics.IgnoreCollision(collider, c, false);
		}
		ignoreColliders.Clear();

		SimplePool.Despawn(gameObject);
	}

	public override void Kill()
	{
		Despawn();

		PlaySoundEffect("Break", true);

		GameManager.instance.CurrentScore += scoreValue;

		if (Size > 1)
		{
			BaseAsteroid[] splitAsteroids = new BaseAsteroid[splitInto];
			Collider[] splitAsteroidColliders = new Collider[splitInto];

			float angleSplit = 360.0f / splitInto;
			int newSize = Size - 1;

			for (int i = 0; i < splitInto; i++)
			{
				BaseAsteroid asteroid = SimplePool.Spawn(GameManager.instance.asteroidPrefab.gameObject, transform.position, transform.rotation).GetComponent<BaseAsteroid>();
				asteroid.transform.Rotate(transform.up, UnityEngine.Random.Range(i * angleSplit, (i * angleSplit) + angleSplit));
				splitAsteroids[i] = asteroid;
				splitAsteroidColliders[i] = asteroid.collider;
			}

			for(int i = 0; i < splitInto; i++)
			{
				splitAsteroids[i].OnSpawn(newSize, splitAsteroidColliders.Where(c => c != splitAsteroids[i].collider).ToList());
			}
		}
	}

	public void OnGameStart()
	{
		Despawn();
	}

	//public override void Teleport(Vector3 newPosition)
	//{
	//	StartCoroutine(TeleportDelay(newPosition));
	//}

	// Play an exit/enter anim when wrapping around map, didn't look/feel great so decided to not use it
	public IEnumerator TeleportDelay(Vector3 newPosition)
	{
		animController.Play("AsteroidShrinkOut");
		yield return new WaitForSeconds(0.1f);
		base.Teleport(newPosition);
	}
}
