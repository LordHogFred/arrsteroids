﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

[Serializable]
public class SoundEffect
{
	public string name;
	[Range(0,1f)]
	public float volume = 1f;
	public AudioClip audioClip;
}

public class BaseEntity : MonoBehaviour, IDamagable
{
	[SerializeField]
	private int maxHealth = 10;
	public int MaxHealth { get { return maxHealth; } }

	[SerializeField]
	private int health = 10;
	public int Health
	{
		get { return health; }
		set
		{
			int prevHealth = health;
			health = value;
			if (HealthChangedEvent != null)
				HealthChangedEvent(prevHealth, health);
		}
	}

	public Action<int, int> HealthChangedEvent;

	/// <summary>
	/// Is the ship currently alive
	/// </summary>
	public bool Alive { get { return health > 0; } }

	public new Collider collider { get; private set; }
	public new Rigidbody rigidbody { get; private set; }
	public Animator animController { get; private set; }
	public AudioSource audioSource { get; private set; }

	public SoundEffect[] soundBank;

	protected virtual void Awake()
	{
		// Cache components
		collider = GetComponent<Collider>();
		rigidbody = GetComponent<Rigidbody>();
		animController = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();

		// Register events
		HealthChangedEvent += OnHealthChanged;
	}

	/// <summary>
	/// Deal damage to the entity
	/// </summary>
	/// <param name="damage"></param>
	public virtual void Damage(int damage)
	{
		Health -= damage;
	}

	/// <summary>
	/// Kill this entity
	/// </summary>
	public virtual void Kill()
	{
		SimplePool.Despawn(gameObject);
	}

	/// <summary>
	/// Fired from HealthChangeEvent
	/// </summary>
	/// <param name="prevHealth"></param>
	/// <param name="currentHealth"></param>
	public virtual void OnHealthChanged(int prevHealth, int currentHealth)
	{
		if (currentHealth <= 0)
		{
			Kill();
		}
	}

	/// <summary>
	/// Attempts to play sound effect with name sfxName from this entities soundbank
	/// </summary>
	/// <param name="sfxName"></param>
	public void PlaySoundEffect(string sfxName, bool remoteAudioSource = false)
	{
		SoundEffect soundEffect = soundBank.First(sfx => sfx.name == sfxName);
		if (soundEffect != null)
		{
			if (remoteAudioSource)
				GameManager.instance.audioSource.PlayOneShot(soundEffect.audioClip, soundEffect.volume);
			else
				audioSource.PlayOneShot(soundEffect.audioClip, soundEffect.volume);
		}
	}

	public virtual void Teleport(Vector3 newPosition)
	{
		transform.position = newPosition;
	}
}
