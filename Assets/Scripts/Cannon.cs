﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CannonGroup
{
	Left,
	Right,
	Front,
	Rear
}

public class Cannon : MonoBehaviour
{
	public CannonGroup cannonGroup;
	public Transform projectileSpawnPoint;
	public Cannonball projectilePrefab;
	public BaseShip owningShip;
	
	public void Fire()
	{
		// TODO: Add some additional force based on speed of ship
		Cannonball cannonball = SimplePool.Spawn(projectilePrefab.gameObject, projectileSpawnPoint.position, projectileSpawnPoint.rotation).GetComponent<Cannonball>();
		cannonball.OnSpawn(this);
	}
}
