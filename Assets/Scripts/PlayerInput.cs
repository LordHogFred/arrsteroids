﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoSingleton<PlayerInput>
{
	// TODO: Create an IControllable and use that instead
	public BaseShip playerShip;

	private Vector3 moveAxis;
	private Vector3 lookAxis;

	private void OnEnable()
	{
	}

	private void Awake()
	{
		moveAxis = Vector3.zero;
		lookAxis = Vector3.zero;
	}

	private void Update()
	{
		if (playerShip == null)
			return;

		moveAxis.Set(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
		lookAxis.Set(Input.GetAxis("HorizontalLook"), 0, -Input.GetAxis("VerticalLook"));

		// Handle movement
		playerShip.HandleMovement(moveAxis);

		// Handle firing cannons
		if(lookAxis.magnitude > 0)
		{
			playerShip.FireCannons(lookAxis);
		}
		else if(Input.GetMouseButton(0))
		{
			playerShip.FireCannons(Vector3.left);
		}
		else if(Input.GetMouseButton(1))
		{
			playerShip.FireCannons(Vector3.right);
		}
	}
}
