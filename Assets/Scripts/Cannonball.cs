﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannonball : MonoBehaviour
{
	public float speed = 5.0f;
	public float despawn = 3.0f;
	public int damageValue = 10;

	private float lifeTime;
	//private Cannon shotBy;

	private void Update()
	{
		transform.position += transform.forward * speed * Time.deltaTime;

		lifeTime += Time.deltaTime;
		if(lifeTime >= despawn)
		{
			Despawn();
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Asteroid")
		{
			IDamagable damagable = other.GetComponent<IDamagable>();
			damagable.Damage(damageValue);
			Despawn();
		}
	}

	public void OnSpawn(Cannon _shotBy)
	{
		lifeTime = 0;
		//shotBy = _shotBy;
	}

	public void Despawn()
	{
		//shotBy = null;
		SimplePool.Despawn(gameObject);
	}
}
