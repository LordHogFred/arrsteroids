﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoSingleton<UIManager>
{
	public GameObject titlePanel;
	public GameObject ingamePanel;
	public GameObject highScorePanel;
	public GameObject gameOverPanel;

	public Text highScoreValueText;

	public Slider healthSlider;
	public Text currentScoreText;

	private void Start()
	{
	}

	public void RegisterEvents()
	{
		GameManager.instance.GameStartEvent += OnStartGame;
		GameManager.instance.GameEndEvent += OnEndGame;
		GameManager.instance.ScoreChangedEvent += OnScoreChanged;
		GameManager.instance.HighScoreChangedEvent += OnHighScoreChanged;
	}

	public void OnStartGame()
	{
		GameManager.instance.playerShip.HealthChangedEvent += OnPlayerHealthChanged;

		titlePanel.SetActive(false);
		gameOverPanel.SetActive(false);
		ingamePanel.SetActive(true);
		highScorePanel.SetActive(false);
	}

	public void OnEndGame()
	{
		GameManager.instance.playerShip.HealthChangedEvent -= OnPlayerHealthChanged;

		gameOverPanel.SetActive(true);
		ingamePanel.SetActive(false);
		highScorePanel.SetActive(true);
	}

	public void OnPlayerHealthChanged(int prevHealth, int currentHealth)
	{
		healthSlider.normalizedValue = (float)currentHealth / (float)GameManager.instance.playerShip.MaxHealth;
	}

	public void OnScoreChanged(int newScore)
	{
		currentScoreText.text = newScore.ToString();
	}

	public void OnHighScoreChanged(int newScore)
	{
		highScoreValueText.text = newScore.ToString();
	}
}
